package CrCM;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;

import CrCM.CircularModulator.DIRECTION;

public class CircularModulatorGUI extends JFrame {

	/**
	 * Base offset of farthest orbit
	 */
	private static final int base = 200;
	
	/**
	 * Number of orbits
	 */
	private static final int parts = 100;
	
	/**
	 * Width of orbit line
	 */
	private static final int width = 10;
	
	/**
	 * Update delay
	 */
	private static final int delay = 50;

	/**
	 * Spin direction of odd orbits
	 */
	private static final DIRECTION orbit1Direction = DIRECTION.CW;
	/**
	 * Spin direction of even orbits
	 */
	private static final DIRECTION orbit2Direction = DIRECTION.CW;

	private static final long serialVersionUID = -6235052160186661958L;

	public CircularModulatorGUI() {
		construct();
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		setVisible(true);
	}

	private void construct() {
		super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		super.setBackground(Color.white);
		super.setSize(640, 640);
		super.setTitle("Circular Modulator");
		JPanel content = new JPanel();
		super.setContentPane(content);
		content.setLayout(new BorderLayout());
		final CircularModulator c = new CircularModulator();
		int width = base / parts;
		DIRECTION[] orbits = new DIRECTION[] { orbit1Direction, orbit2Direction };
		for (int i = 0; i < parts; i++) {
			c.addRubus(base - (i * width), (i + 1) * width, CircularModulatorGUI.width, orbits[i % 2], i + 1);
		}
		content.add(c.getComponent(), BorderLayout.CENTER);
		c.getComponent().setBackground(Color.white);
		new Thread() {
			@Override
			public void run() {
				synchronized (this) {
					while (true) {
						try {
							this.wait(delay);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						c.update();
					}
				}
			}
		}.start();
	}
}
