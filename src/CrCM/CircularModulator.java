package CrCM;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JPanel;

public class CircularModulator {

	private JPanel cmp;
	private final List<Rubus> rbs = new ArrayList<>();

	private static final Comparator<Rubus> rcmp = new Comparator<Rubus>() {

		@Override
		public int compare(Rubus r1, Rubus r2) {
			return r1.getOffset() - r2.getOffset();
		}

	};

	public CircularModulator() {
		this.cmp = new JPanel() {
			private static final long serialVersionUID = 4604111223918380501L;

			@Override
			public void paint(Graphics g) {
				super.paint(g);
				int w = super.getWidth();
				int h = super.getHeight();
				handlePaint(w > h ? h : w, w, h, g);
			}
		};
	}

	public static class Rubus {
		private int offset;
		private int size;
		private int psize;
		private DIRECTION dir;
		private int speed;

		private int position = 0;

		public void update() {
			position = (dir == DIRECTION.CCW ? position + speed : position - speed) % 360;
			position = toAngle(position);
		}

		public int getCurrentPosition() {
			return position;
		}

		private static int toAngle(int i) {
			i %= 360;
			return i < 0 ? 360 + i : i;
		}

		private Rubus(int a, int b, int c, DIRECTION d, int e) {
			this.offset = a;
			this.size = b;
			this.psize = c;
			this.dir = d;
			this.speed = e;
			position = toAngle(90-psize+(psize/2));
		}

		public DIRECTION getTurnDirection() {
			return dir;
		}

		public int getTurnSpeed() {
			return speed;
		}

		public int getOffset() {
			return offset;
		}

		public int getSize() {
			return size;
		}

		public int getPercentageSize() {
			return psize;
		}
	}

	public static enum DIRECTION {
		CW, CCW;
	}

	public void addRubus(int off, int percentagesize, int size, DIRECTION dir, int spinspeed) {
		rbs.add(new Rubus(off, size, percentagesize, dir, spinspeed));
		Collections.sort(rbs, rcmp);
		detectMaxHeight();
	}

	private void detectMaxHeight() {
		int max = 0;
		for (Rubus r : rbs) {
			int q = (r.getSize()) + r.getOffset();
			if (q > max) {
				max = q;
			}
		}
		maxHeight = max + 20;
	}

	private int maxHeight = 100;

	private void handlePaint(int size, int x, int y, Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		double mc = (double) size / (double) maxHeight;
		int xc = x / 2;
		int yc = y / 2;
		for (Rubus r : rbs) {
			g2d.setStroke(new BasicStroke((int) (r.getSize() / mc)));
			int s = (int) ((r.getSize() + r.getOffset()) * mc);
			int rsh = s / 2;
			g.drawArc(xc - rsh, yc - rsh, s, s, r.getCurrentPosition(), r.getPercentageSize());
		}
	}

	public JComponent getComponent() {
		return cmp;
	}

	public void update() {
		for (Rubus r : rbs) {
			r.update();
		}
		cmp.repaint();
	}

}
